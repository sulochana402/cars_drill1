const inventory = require("../inventoryObject");
const problem5 = require("../problem5");

const result = problem5(inventory, 2000);
console.log(result);
console.log("Number of cars older than year 2000:", result.length);

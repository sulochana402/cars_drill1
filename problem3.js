// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortCarModelsAlphabetically(inventory) {
  if (Array.isArray(inventory)) {
    let sortCarModels = [];
    for (let index = 0; index < inventory.length; index++) {
      sortCarModels.push(inventory[index].car_model);
    }
    return sortCarModels.sort();
  } else {
    return (inventory = []);
  }
}
module.exports = sortCarModelsAlphabetically;
